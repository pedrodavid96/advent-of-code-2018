open System.IO;
open System.Linq;

let lines = File.ReadLines("input")

printfn "Star 1"

let hasCharCountOf count listOfCharCounts = listOfCharCounts |> Seq.filter (Seq.exists (fun elem -> snd elem = count)) |> Seq.length

let charCounts = lines |> Seq.map (Seq.countBy id)
let linesWithTwoOfSame = hasCharCountOf 2 charCounts
let linesWithThreeOfSame = hasCharCountOf 3 charCounts

printfn "%d" (linesWithTwoOfSame * linesWithThreeOfSame)

printfn "Star 2"

let linesCopy = lines.ToList();

let res = lines
        |> Seq.map (fun line ->
                let minDiff = linesCopy
                            |> Seq.filter(fun otherLine -> line <> otherLine) 
                            |> Seq.map(fun otherLine -> 
                                let test = Seq.zip line otherLine
                                test)
                            |> Seq.map(fun joinedLines -> 
                                let numOfDifferentChars = joinedLines 
                                                        |> Seq.filter(fun chars ->
                                                            let (a, b) = chars
                                                            a <> b)
                                                        |> Seq.length
                                (joinedLines, numOfDifferentChars))
                            |> Seq.minBy snd
                minDiff)
        |> Seq.minBy snd

let result = fst res
            |> Seq.filter(fun chars ->
                let (a, b) = chars
                a = b)
            |> Seq.map fst
            |> Seq.toArray
let strResult = new string(result)
printfn "%s" strResult
