import System.IO

doubleMe x = x + x

counts line = group line

main = do
    contents <- readFile "input"
    map counts contents 
    putStr contents
