function greeter(person) {
    return "Hello, " + person
}

const user = "Jane User"

console.log(greeter(user))

import * as fs from 'fs'

// fs.readFileSync('input', 'utf8')
//     .split(/\r\n|\r|\n/)
//     .map(toEntry)

// toEntry(fs.readFileSync('input', 'utf8')
//     .split(/\r\n|\r|\n/)[0])
// toEntry(fs.readFileSync('input', 'utf8')
//     .split(/\r\n|\r|\n/)[1])
toEntry(fs.readFileSync('input', 'utf8')
    .split(/\r\n|\r|\n/)[2])
toEntry(fs.readFileSync('input', 'utf8')
    .split(/\r\n|\r|\n/)[3])
toEntry(fs.readFileSync('input', 'utf8')
    .split(/\r\n|\r|\n/)[13])


class MDate {
    constructor(public month: number, public day: number) {}
}

function toEntry(line: string): Entry {
    const entryPattern = /\[(.*)\] (falls asleep|wakes up|Guard #(\d+) begins shift)/
    const [, dateString, stateChange, id] = entryPattern.exec(line) || ["", "", "", ""]
    const datePattern = /(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d)/
    const [,, month, day,  hour, minute] = datePattern.exec(dateString)
    console.log(new MDate(parseInt(month), parseInt(day)))
    console.log(stateChange)
    console.log(id)
    return {}
}


class Entry {

}